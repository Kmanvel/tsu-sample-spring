package com.manvelux.film.controller;

import com.manvelux.film.model.Film;
import com.manvelux.film.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/film")
public class FilmController {
    private final FilmRepository repository;

    @Autowired
    public FilmController(FilmRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public Film add(@RequestBody Film film){
        return repository.save(film);
    }

    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
    public void delete(@RequestBody Film film){
        repository.delete(film);
    }

    @RequestMapping(path = "/edit", method = RequestMethod.PUT)
    public void edit(@RequestBody Film film){
        repository.save(film);
    }

    @RequestMapping(path = "/all/{page}/{size}", method = RequestMethod.GET)
    public ResponseEntity<?> getAll(@PathVariable int page, @PathVariable int size){
        return ResponseEntity.ok(repository.findAll(PageRequest.of(page,size)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getOne(@PathVariable long id) {
        Film f = repository.findById(id).orElse(null);
        return ResponseEntity.ok(f);
    }
}
